let user = new Object();
console.log(user); //{}
let user1 = {};
console.log(user1); //{}

let data ={
    name: 'anything',
    age: 76,
    isAdmin: true
}
console.log(data.name);
console.log(data.age);
console.log(data.isAdmin);
user.isAdmin = false;
console.log(user.isAdmin);

delete data.age;
console.log(data);

let user11 = {
    // cheque: "icici",
    'jobs st': "unknown",
    "xyz": "chewing",
    "name": 23,
    "age": "90"
}
console.log(user11);

for( let d in user11){
    console.log(d);
}

console.log(user11.just = "nope");


let fruit = 'apple';
//console.log(bag);
fruit = 'orange';
fruit = 'gold';
let bag = {
    [fruit]: 5
};
console.log(bag);
fruit = 'silver';
console.log(bag);

let ab = {
    name: 'st',
    ag: 34
}
console.log("name" in ab);
console.log(!"name" in ab);

for(let s in ab){
    console.log(s);
}
for(let t in ab){
    console.log([t]);
}

let codes = {
    '+4': 'no tent',
    '+1': 'goes to',
    '+2': 'comes to',
    '+5': 'becomes',
    '+7': 'let it'
}
for(let cd in codes){
    console.log(cd);
}

let rtv = {
    a: 'a',
    c: 'c',
    b: 'b',
    z: 'z',
    d: 'd'
}
console.log(rtv);

let cds = {
    10: 10,
    40: 40,
    1: 3,
    2: 2,
    8: 8
}
console.log(cds);
for(let ds in cds){
    console.log(ds);
}

let person = {
    name: 'Subway'
};
let permission1 = {
    canEdit: true,
};
let permission2 = {
    cenClear: true,
}
Object.assign(person, permission1, permission2);
console.log(person);

person.pss = permission1;
console.log(person);

let ps = {
    name: 'jon',
    sizes: {
        height: 23,
        width: 54
    }
};
let clone = Object.assign({}, ps);
console.log(clone);
console.log(clone.sizes == ps.sizes);

let check = {
    name: "ps",
    age: 45,
    getData(){
        return this.name;
    }
}
let admin = check;
console.log(admin.getData());

let foot = { name: 'guavava'};
let gst = { name: 'pstret'};
function checkit(){
    return this.name;
};
foot.f = checkit;
gst.f = checkit;

console.log(foot.f());
console.log(gst.f());
console.log(this);

function ispossible(){
    return;
}
console.log(ispossible());

function target(){
    new.target;
}
console.log(target());
console.log(new target());

function Cancel(name){
    if(!new.target){
        return new Cancel(name);
    }
    this.name = name;
}
let John = Cancel("john");
console.log(John.name);

function Accumulator(value){
    this.val = value;
    this.read = function(){
        this.val += 10;
    };
}

let accumulator = new Accumulator(1);
accumulator.read();
accumulator.read();
console.log(accumulator.val); 

// let cost = null;
// // cost?.(console.log('present'));
// let pd = null;
// let x=0;
// pd?.sayHi(x++);
// console.log(x);

let di = Symbol("di");
console.log(di);

// Symbol
let id = Symbol('id');
let df = {
    name: 'Jan',
    age: 30,
    [id]: 123
};
for(let key in df){
    console.log(key);
}
console.log('Direct ',df[id]);

let billion = 2e8;
console.log(billion);
let negative = 2e-5;
console.log(negative);

// hex, binary and octal
console.log(0xff);
console.log(0b1111);
console.log(0o323);

let num = 244;
console.log( num.toString(16));
console.log( num.toString(2));

console.log(Math.ceil(3.1));
console.log(Math.ceil(-3.1));
console.log(Math.floor(-3.1));

let fg = '100';
console.log(+fg);

console.log("\u00A9");
console.log("\u{20331}");
console.log("\u{1F60D}");

console.log('I\'m the walrus!');

let str = 'As sly as fox, as string as ox';
// let target = 'as';
let pos = 0;

let str1 = 'Widget with id';
if(str1.indexOf('Widget') != -1) {
    console.log('Found it');
}

let got = 'Widget';
if(~str.indexOf('Widget')){
    console.log('Found it!!');
}
console.log(got.endsWith('get'));
console.log(got.startsWith('get'));

console.log(got.slice(0,2));
console.log(got.substr(-4,2));
let arr = ['a','b','c'];
console.log(arr);
arr.push('d');
console.log(arr);
arr.pop();
console.log(arr);
arr.shift();
console.log(arr);
arr.unshift('z');
console.log(arr);
arr = [1, 2, 3,4,5,6,7];
// for(let a=0; a<arr.length;  a++){
//     console.log(arr);
//     arr.shift();
// }

let ptr = ['abc', 'gst', 'poi', 'lkj'];
let removed = ptr.splice(0,2);
console.log(removed);

let arrayLike = {
    0: 'something',
    length: 1
}

let dsr = ptr.concat(arrayLike);
console.log(dsr);
console.log(ptr);

["Bilbo", "Gandalf", "Nazgul"].forEach((item, index, array) => {
    console.log(`${item} is at index ${index} in ${array}`);
  });

console.log(dsr.indexOf('lkj'));
console.log(dsr.includes('lkj'));
console.log(dsr.includes('kj'));

let users = [
    {id: 1, name: "John"},
    {id: 2, name: 'Pete'},
    {id: 3, name: 'Left'}
];
let pll = users.find(item => item.id == 1);
console.log(pll);

// console.log(getMaxSubSum([-1,2,3,-4]) == 5);