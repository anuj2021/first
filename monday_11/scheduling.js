//setTimeOut
//setInterval
//setImmediate

let func = setTimeout(function(){ 
    console.log('Who is this ')
}, 1000);

setTimeout(() => console.log('Hello'), 1000);
//it sets a timer which executes a function or specified 
//piece of code once the timer expires.

//-----------------------------------------------------------

//setInterval() ----> Execute a specified block of code repeatedly 
//with a fixed time delay between each call.

function displayTime() {
    let date = new Date();
    let time = date.toLocaleTimeString();
    console.log(time);
 }
 
//const createClock = setInterval(displayTime, 1000);

let arr = [ {
    id : 1,
    name : 'a',
    subject : 'sd',
    teacher : 'Teach01'
}, {
    id : 2,
    name : 'a',
    subject : 'sd',
    teacher : 'Teach02'
}, {
    id : 3,
    name : 'a',
    subject : 'sd',
    teacher : 'Teach01'
}, {
    id : 4,
    name : 'a',
    subject : 'sd',
    teacher : 'Teach03'
}, {
    id : 5,
    name: 'b',
    subject : 'ds',
    teacher : 'Teach03'
}];
let info = new Map();
let ans = arr.map(ele => {
    if(info.has(ele.teacher)){
        info.get(ele.teacher).push(ele);
    } else {
        info.set(ele.teacher, [ele])
    }
});
console.log('-----------------');
console.log(info);
console.log(info.get('Teach03'));
console.log('#################');
for ( let [key, value] in Object.entries(info)){
    console.log(key, '--',value);
}
let ps = [];
const converter = (key, value) => {
    console.log('-------++++++++++-------');
    console.log(key, value);
    //ps.push(key,value);
}
let gs = [];
info.forEach((value, key) => {
    console.log(value, key);
    // gs.push(
    //     JSON.stringify(key) = JSON.stringify(value)
    // );
    console.log(JSON.stringify(key));
    // console.log(JSON.parse(key));
    converter(key, value);
});
console.log(gs);
console.log(ps);
const obje = Object.fromEntries(info);
console.log(obje);