let arr = [ {
    id : 1,
    name : 'a',
    subject : 'sd',
    teacher : 'Teach01'
}, {
    id : 2,
    name : 'a',
    subject : 'sd',
    teacher : 'Teach02'
}, {
    id : 3,
    name : 'a',
    subject : 'sd',
    teacher : 'Teach01'
}, {
    id : 4,
    name : 'a',
    subject : 'sd',
    teacher : 'Teach03'
}, {
    id : 5,
    name: 'b',
    subject : 'ds',
    teacher : 'Teach03'
}];
let info = new Map();
let ans = arr.map(ele => {
    if(info.has(ele.teacher)){
        info.get(ele.teacher).push(ele);
    } else {
        info.set(ele.teacher, [ele])
    }
});
console.log(info);
console.log('---------------------------------');

const obje = Object.fromEntries(info);
console.log(obje);

for(let ps in obje){
    console.log(obje[ps]);
}
console.log(JSON.stringify(obje));
