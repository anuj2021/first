//Strings are immutable in javascript
let str = 'Hi';
console.log(str);

str = str[0] + 'o'; //But we can replace the string
console.log(str);

console.log('Interface'.toUpperCase()); //whole to uppercase
console.log('iNGTRGFF'.toLowerCase()); //whole to lowercase

let stri = 'Widget with id';
console.log(stri.indexOf('Widget'));
console.log(stri.indexOf('widget'));
console.log(stri.indexOf('id'));

let dup = 'stringify';
console.log(dup.substring(2,6));

const st = '  local  local  ';
console.log(st.trim());

console.log('XTF'[2]);
