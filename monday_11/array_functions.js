//find
//filter
//map
//reduce

const array1 = [2, 6, 5, 3, 7];
const found = array1.find( element => element >= 5);

console.log(found);
// The find() method returns the value of the first element in the 
// provided array that satisfies the provided testing function. If 
// no values satisfies the testing function, undefined is returned.

function isPrime(element, index, array) {
    let start = 2;
    while (start <= Math.sqrt(element)) {
      if (element % start++ < 1) {
        return false;
      }
    }
    return element > 1;
}
  
console.log([4, 6, 8, 12].find(isPrime)); // undefined, not found
console.log([4, 5, 8, 12].find(isPrime)); // 5
//----------------------------------------------------------

const words = ['spray', 'limit', 'elite', 'exuberant', 'destruction', 'present'];
const result = words.filter(word => word.length > 6);
console.log(result);
// The filter() method creates a new array with all elements 
// that pass the test implemented by the provided function.

const array = [-3, -2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13];
function isPrime(num) {
  for (let i = 2; num > i; i++) {
    if (num % i == 0) {
      return false;
    }
  }
  return num > 1;
}
console.log(array.filter(isPrime)); // [2, 3, 5, 7, 11, 13]

//-------------------------------------------------------------------
const arr = [1, 4, 9, 16];
const map1 = arr.map(x => x*2);
console.log(map1);

// The map() method creates a new array populated with the results 
// of calling a provided function on every element in the calling array.

let numbers = [1, 4, 9]
let roots = numbers.map(function(num) {
    return Math.sqrt(num)
})
console.log(roots);
//--------------------------------------------------------------------

const array3 = [1, 2, 3, 4];
const reducer = (accumulator, currentValue) => accumulator + currentValue;
console.log(array3.reduce(reducer));
// 5 + 1 + 2 + 3 + 4
console.log(array3.reduce(reducer, 5));

// The reduce() method executes a reducer function (that you provide) 
// on each element of the array, resulting in single output value.

let total = [ 0, 1, 2, 3 ].reduce(
    ( accumulator, currentValue ) => accumulator + currentValue,
    0
);
console.log(total);
