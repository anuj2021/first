// try{
//     setTimeout(function(){
//         noSuchVariable;
//     }, 100);
// } catch(err) {
//     alert('Wont work');
// }

// setTimeout(function() {
//     try{
//         noSuchVariable;
//     } catch (err) {
//         console.log('Error caught here!!!');
//         console.log(err.message);
//         console.log(err.name);
//         console.log(err.stack);
//         // console.log(err);
//     }
// }, 100);


let json = '{"name": "John", "age":30}';
let user = JSON.parse(json);//convert the text to js object
console.log(user);
console.log(user.name);
console.log(user.age);

let kson = '{"age" : 40}';
try{
    let usr = JSON.parse(kson);
    if(!usr.name){
        throw SyntaxError("Incomplete Data");
    }
} catch(err) {
    console.log(err.name);
    console.log(err.message);
}

let lson = '{"age": 30}';
try{
    let sr = JSON.parse(lson);
    if(!sr.name){
        throw new SyntaxError("Incoml")
    } }
    catch(err){
        console.log(err);
}

let hello = "Hello World";
let sws = hello;
console.log(hello+"  -- "+sws);
hello = "Guy";
console.log(hello+ " -- "+ sws);

let $ = 34;
let _ = 9;
console.log($ + " "+ _);

let la;
console.log(la);
const COLOR_RED = '#44f4';
console.log(COLOR_RED);

let ourPlanet = 'Earth';
let currentVisitor = 'New User';
let outPlanetName = 'Earth';
const currentUserName = 'Other User';
const BIRTHDAY = '01.11.1999';

let double = (val) => val*2;
console.log(double(33));
console.log(typeof(double(3)));

let good = () => "Hello";
good();
let age = 18;
let welcome = age < 50 ? () => 'Hello' : () => 'Not Hello';
console.log(welcome(age));
console.log(welcome(90));

let ask = (question, yes, no) => {
    if(question){
        yes()
    }
    else {
        no()
    }
};
ask(false, () => console.log("YES"), () => console.log("NO"));

function defer(f, ms){
    return function(){
        setTimeout(() => f.apply(this, arguments), ms);
    };
}
function sayHi (who) {
    console.log('Hello '+who);
}
let sayHiDeffered = defer(sayHi, 2000);
sayHiDeffered("John");

let words = {};
words["kind"]=true;
console.log(words["kind"]);
delete words["kind"];
console.log(words);

let fruit = 'Apple';
fruit = 'Orange';
let beg = {
    [fruit]: 5
}
console.log(beg["Apple"]);
fruit = 'Orange';
console.log(beg['Orange']);

const football = {
    name: 'Game',
    age: 120
}
let key = 'name';
console.log(football[key]);
console.log(football.key);
const cricket = {
    [key]: 'Valyue'
};
console.log(cricket.name);

let getUser = (name, age) => ({
    name,
    age
});
let data = getUser("Home", 98);
console.log(data);

let obj = {
    for: 23,
    let: 23,
    return: 23
}
console.log(obj.for+obj.let+obj.return);

console.log("for" in obj);
console.log("return" in obj);

for(let st in obj){
    console.log(obj[st]);
}

let re = {};
re.name = 'John';
re.surname = 'Smith';
console.log(re);
re.name = 'jjjjjjj';
console.log(re);
delete re.name;
console.log(re);

let salaries = {
    John: 100,
    Ann: 160,
    Pete: 130
  }
  let sum = 0;
for(let gst in salaries){
    sum += salaries[gst];
}
console.log(sum);

const menu ={
    width: 100,
    height: 200,
    title: 'My Menu'
};
let multiplyNumeric = (menu) => {
    for(let lk in menu){
        if( typeof(menu[lk]) == 'number'){
            menu[lk] = 2*menu[lk];
        }
    }
}
multiplyNumeric(menu);
console.log(menu);

const student = {
    name: 'John',
    age: 30,
    isAdmin: false,
    courses: ['html', 'css', 'js'],
    wife: null
};
let son = JSON.stringify(student);
console.log(typeof(son));
console.log(son);
console.log(JSON.stringify(true));
console.log(typeof(JSON.stringify(true)));
console.log("This");
console.log(JSON.stringify([1,2,3,4]));

let room = {
    number: 34
};
let meetup = {
    title: "conference",
    participants: ["John", "Ann"]
};
meetup.place = room;
room.occupiedBy = meetup;
console.log(meetup);
console.log(room);
// JSON.stringify(room);
console.log(JSON.parse(son));

let streingth = {
    onClick(){
        console.log('hello')
    },
    [Symbol["id"]]: 'cancel',
    dom: undefined
}
console.log(streingth);
console.log(JSON.stringify(streingth));

let roo = {
    number: 23
  };
  
  let meetu = {
    title: "Conference",
    participants: [{name: "John"}, {name: "Alice"}],
    place: roo // meetup references room
  };

  roo.occupiedBy = meetu;
console.log(JSON.stringify(meetu, ['title', 'participant']));

const get = {
    name: "john",
    "surname": 'smith',
    'isAdmin': false,
    "birthday": new Date(2000,2,3),
    "friends": [0,1,2,3]
}
console.log(get);

let us = {
    name: "John Smith",
    age: 35
  };
  
let user2 = JSON.parse(JSON.stringify(us));
console.log(user2);

function compareNumeric(a, b) {
    if (a > b) return 1;
    if (a == b) return 0;
    if (a < b) return -1;
  }
  
  let arr = [ 1, 2, 15 ];
  
  arr.sort(compareNumeric);
  
  console.log(arr);  // 1, 2, 15

  let go = {
      name: "something",
      age: 00,
      toString(){
          return `{name: ${this.name}, age: ${this.age}}`
      }
  };
console.log(go.toString());

let z = JSON.stringify(1);
console.log(typeof(z));
let jh = 'sd';
let c = JSON.stringify(23[re, jh, 3]);
console.log(c);

let up = {
    title: 'conference',
    participants : [{ name: 'something'}, { name: 'rohit'}],
    place: 'Delhi'
};
console.log(JSON.stringify(up, ['title', 'participants']))

let p = {
    title: "place",
    date: new Date(Date.UTC(2017, 0, 1))
};
console.log(p);

const numbers = "[1,2,3,4,5]";
console.log(JSON.parse(numbers));
console.log(numbers);
//--------------------------------------------
const str = '{"title":"Conference","date":"2017-11-30T12:00:00.000Z"}';

let etup = JSON.parse(str, function(key, value) {
  if (key == 'date') return new Date(value);
  return value;
});

console.log( etup.date.getDate() ); // now works!

const ar = [1, 2, 5,4,3];
ar.sort( function (a, b) {
    return a - b ;
});
console.log(ar);

const zebra = 'a,b,c,d,e';
console.log(zebra.split(',')); //split works on string only

const zebr = 'a,b,c,d,e,f';
console.log(zebr.split(',', 2)); //last numeic value is the considerable size

const tiger = [ 'a', 'b', 'c', 'p'];
console.log(tiger.join(';'));

let a = [1, 2, 3, 4, 5];

let result = a.reduce((sum, current) => sum + current, 0);

// alert(result); // 15
let boss = [1, 2, 3];
let rs = boss.reduce((sum, curr) => sum + curr, 0);
console.log(rs);

const pr = [NaN];
console.log(pr.indexOf(NaN));
console.log(pr.includes(NaN));

const ers = [
    {id: 1, name: "John"},
    {id: 2, name: "Pete"},
    {id: 3, name: "Mary"}
  ];
  
let userd = ers.find(item => item.id == 1);
  
console.log(userd); 

let rd = ers.filter(item => item.id < 3); //filter returns array of object
console.log(rd);

let lengths = ["B", "G", "N"].map(item => item.length) //map returns array of elements
console.log(lengths);

const ay = {
    minAge: 18,
    maxAge: 23,
    canJoin(user){
        return user.age >= this.minAge && user.age < this.maxAge;
    }
};
let usu = [
    {age: 15},
    {age: 20},
    {age: 30},
    {age: 40}
];
let sldrs = usu.filter(ay.canJoin, ay);
console.log(sldrs.length);

const newproduct = `{ 
    name: 'smith',
    "surname" : "John,
    'isAdmin': false,
    "friends" : [1,2,3,4]
}`;
let zoo;
console.log(zoo = JSON.parse(JSON.stringify(newproduct)));
// for(let oo in zoo){
//     console.log(oo);
// }
console.log(newproduct);

console.log(typeof {});
console.log(typeof []);
console.log(Array.isArray({}));
console.log(Array.isArray([]));

const public = [1, 2, 3, 4, 5];
let gs = public.reduce((sum, current) => sum + current);
console.log(gs);
let crickets = () => "Goods";
console.log(crickets());
