// Solutions to callback hell
// There are four solutions to callback hell:

// - Write comments
//-  Split functions into smaller functions
//-  Using Promises
//-  Using Async/await

const gettype = () =>{
    return 20;
}

const cookedType = () => {
    return 20;
}

const getBrd = () => {
    return 25;
}

const putCoockedTypebrd = (cookedType, brd) => {
    return cookedType + brd;
}

const serve = (item) => {
    console.log("at last this item:-", item);
}

const makeBurger = async () => {
    const cc = await gettype();
    const cookedType = await cookedType();
    const brd = await getBrd();
    const burger =  await putCoockedTypebrd(cookedType, brd);
    return burger;
}
makeBurger().then(item => serve(item));

new Promise((resolve, reject) => {
    console.log('Initial');

    resolve();
})
.then(() => {
    throw new Error('Something failed');

    console.log('Do this');
})
.catch(() => {
    console.error('Do that');
})
.then(() => {
    console.log('Do this, no matter what happened before');
});