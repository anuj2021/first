// try{
//     localStorage;
// } catch (err) {
//     console.log(err);
// }

// try{
//     setTimeout( function() {
//         //noSch;
//     }, 1000);
// } catch (err){
//     console.log("Wont work");
// }

// try {
//     alfa;
// } catch (error) {
//     console.log('Error Here');
// } finally {
//     console.log("It always executes");
// }

// try {
//     return 'true';
// } catch (error) {
//     console.log('No error');
// } finally {
//     console.log("Always executes");
// }

// For instance, when there’s a return inside try..catch. The finally 
// clause works in case of any exit from try..catch, even via the return
//  statement: right after try..catch is done, but before the calling 
//  code gets the control.

let data = [{
    empid: 1,
    empname: 'ss'
}, {
    empid: 2,
    empname: 'ss'
}, {
    empid: 3,
    empname: 'ss'
}, {
    empid: 3,
    empname: 'ss'
}];
let ssdt = Object.keys(data);
console.log(data);
console.log(ssdt);
let ds = ssdt.map((ele) => data[ele]);
console.log(ds);


new Promise((resolve, reject) => {
    console.log('Initial');

    resolve();
})
.then(() => {
    throw new Error('Something failed');

    console.log('Do this');
})
.catch(() => {
    console.error('Do that');
})
.then(() => {
    console.log('Do this, no matter what happened before');
});