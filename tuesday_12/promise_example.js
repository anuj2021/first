function doTask1(){
    // setTimeout(() => {
        return "Hello";
    // });
};

function doTask2(valu){
    // setTimeout(() => {
        return "Guys";
    // });
};

function doTask3(vlvl){
    return "Today";
}

async function init(){
    const res1 = await doTask1();
    console.log(res1);

    const res2 = await doTask2(res1);
    console.log(res2);

    const res3 = await doTask3(res2);
    console.log(res3);
    return res3;
}
init();