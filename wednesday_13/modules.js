let myGradeCalculate = (function() {
    let myGrade = [10, 30, 80, 90, 40];
    let average = function(){
        let total = myGrade.reduce((accumulator, item) => accumulator + item, 0);
        return "Your Average is: "+total/myGrade.length+'.';
    };
    let failing = function(){
        let failingGrades = myGrade.filter((item) => item<50);
        return "You failed: "+failingGrades.length+' times';
    };
    return {
        average: average,
        failing: failing
    }
})();

console.log(myGradeCalculate.average());
console.log(myGradeCalculate.failing());

// import * as co from './fles.js';
let cc = require('./fles.js');
console.log(cc.count);
console.log(cc.increment());
console.log(cc.increment());
console.log(cc.increment());
console.log(cc.decrement());

// import { myLogger, myNumber } from './ada.js';
const st = require('./ada.js');
console.log(st.myLogger());
console.log(st.myNumber);
console.log(st.Alligator());

