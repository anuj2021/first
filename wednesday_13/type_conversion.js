// String Conversion

console.log(true, "  ", typeof true);
console.log(String(true));

//Numeric Conversion
//-------It happens in mathematical functions and expressions automatically

console.log("6"/"2");
let gs = "6"/"2";
console.log(typeof gs);


console.log(Number("you are wrong"));
console.log(Number(undefined));
console.log(Number(null));
console.log(Number(1));
console.log(Number("  "));
console.log(Number("1"));
console.log(Number("as12"));

//Boolean conversion

console.log(Boolean(1));
console.log(Boolean(0))
console.log(Boolean("  helo "));
console.log(Boolean("   "));