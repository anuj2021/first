let count = 0;

let increment = () => ++count;
// function increment() {
//     ++count;
// }
let decrement = () => --count;
// export function decrement() {
//     --count;
// }
module.exports = {
    count: count,
    increment: increment,
    decrement: decrement
}
