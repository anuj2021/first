// alert( 'Hello, world!' );
// let a=3;
// let b=4;
// b ?? a;
const user = {
    name: 'Pete'
}
console.log('Here is before updated:- ', user.name);

user.name="Changed";

console.log('New Value', user.name);

//---------------

console.log('\n');

function marry(man, women){
    women.husband = man;
    man.wife = women;

    return {
        father: man,
        mother: women
    }
}

let family = marry({
    name: "John"
}, {
    name: "Ahn"
})
console.log(family);